var keystone = require('keystone');

var ContactMsg = keystone.list('ContactMsg');

/**
 * List ContactMsg's
 */
exports.list = function(req, res) {
  ContactMsg.model.find(function(err, items) {

    if (err) return res.json({ err: err });

    res.json({
      contactMsg: items
    });

  });
}

/**
 * Create a ContactMsg
 */
exports.create = function(req, res) {

  var item = new ContactMsg.model(),
    data = (req.method == 'POST') ? req.body : req.query;

  item.getUpdateHandler(req).process(data, function(err) {

    if (err) return res.json({ error: err });

    res.json({
      contactMsg: item
    });

  });
}
