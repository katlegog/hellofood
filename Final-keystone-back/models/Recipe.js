var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Recipe Model
 * ==========
 */
var Recipe = new keystone.List('Recipe');

Recipe.add({
  title: {  type: String, required: true , initial: false},
  category: { type: String, required: true , initial: false},
  ingredients: { type: String, required: true, initial: false },
  cookingTime: { type: Types.Number, required: true, initial: false },
  prep:  { type: String , required: true, initial: false}
});


/**
 * Registration
 */
Recipe.register();
