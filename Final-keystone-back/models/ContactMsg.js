var keystone = require('keystone');
var Types = keystone.Field.Types;

  /**
   * Contact Model
   * ==========
   */
  var ContactMsg = new keystone.List('ContactMsg');

  ContactMsg.add({
    name: { type: Types.Name, required: true, initial: false },
    email: { type: Types.Name, required: true, initial: false },
    msg: { type: Types.Name, required: true , initial: false},

  });


  /**
   * Registration
   */
  ContactMsg.register();
