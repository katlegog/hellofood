import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';

import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { TheRecipeListComponent } from './the-recipe-list/the-recipe-list.component';
import { GalleryComponent } from './content/gallery/gallery.component';
import { NewRecipeFormComponent } from './the-recipe-list/new-recipe-form/new-recipe-form.component';
import { RecipeDisplayComponent } from './the-recipe-list/recipe-display/recipe-display.component';
import { RecipeService } from './the-recipe-list/common/recipe.service';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    TheRecipeListComponent,
    GalleryComponent,
    NewRecipeFormComponent,
    RecipeDisplayComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule {}
