import { Component,Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NgModule } from '@angular/core';

import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';


import { Message } from './message';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})


export class FormComponent implements OnInit {
   @Input() title = `Information`;

   MsgPost: Observable<any>;
   // private url = "http://localhost:3000/api/enquiry";
   // private url = "http://localhost:3000/recipes";


  newForm() {
    this.model = new Message();
  }

  model = new Message();

  submitted = false;

  onSubmit() { this.submitted = true; }

  constructor() { }

  ngOnInit() {
  }

}
