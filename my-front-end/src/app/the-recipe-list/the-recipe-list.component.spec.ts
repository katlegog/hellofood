import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheRecipeListComponent } from './the-recipe-list.component';

describe('TheRecipeListComponent', () => {
  let component: TheRecipeListComponent;
  let fixture: ComponentFixture<TheRecipeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheRecipeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheRecipeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
