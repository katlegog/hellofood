import { Component, OnInit, OnDestroy } from '@angular/core';
import { TheRecipeListComponent } from '../the-recipe-list.component';

import { HttpClient } from '@angular/common/http';
import { Recipe } from '../common/recipe';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpHeaders } from '@angular/common/http'
import { Observable, Subject, throwError } from 'rxjs';
import { NewRecipeFormComponent } from '../new-recipe-form/new-recipe-form.component';
// import { map, catchError, retry, subscribeOn } from 'rxjs/operators';
import { RecipeService } from '../common/recipe.service';

@Component({
  selector: 'app-recipe-display',
  templateUrl: './recipe-display.component.html',
  styleUrls: ['./recipe-display.component.scss']
})
export class RecipeDisplayComponent implements OnInit, OnDestroy {
  recipeTemp: any;

  constructor(private recipeService: RecipeService) {
  }

  ngOnInit() {
    this.recipeService.getRecipes()
    .subscribe((response: any) => {
      this.recipeTemp = response.recipe;
    });
  }
  ngOnDestroy() {
    console.log("destroying ")
  }

}
