import { Component, OnInit } from '@angular/core';
import { TheRecipeListComponent } from '../the-recipe-list.component';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpHeaders } from '@angular/common/http'
import { Observable, Subject, throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';
import { Recipe } from '../common/recipe';
import { RecipeService } from '../common/recipe.service';


@Component({
  selector: 'app-new-recipe-form',
  templateUrl: './new-recipe-form.component.html',
  styleUrls: ['./new-recipe-form.component.scss']
})
export class NewRecipeFormComponent implements OnInit {
  recipeTemp: any;
  recipePost: Observable<any>;
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Credentials': 'true'
    })
  };

  profileForm = new FormGroup({
    title: new FormControl(''),
    category: new FormControl(''),
    cookingTime: new FormControl(''),
    ingredients: new FormControl(''),
    prep: new FormControl(''),
  });

  private url = "http://localhost:3000/api/recipe";

  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
  }

  onSubmit(profileForm) {
    const data: Recipe = {
      title: this.profileForm.value.title,
      category: this.profileForm.value.category,
      ingredients: this.profileForm.value.ingredients,
      cookingTime: this.profileForm.value.cookingTime,
      prep: this.profileForm.value.prep
    }
    this.recipeService.createRecipe(data);
    // this.http.post(this.url, data).subscribe(response => {
    //   console.log(response);
    // })
  }
}
