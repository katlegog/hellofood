export class Recipe {
  title: string;
  category: string;
  ingredients: string;
  cookingTime: number;
  prep: string;

  constructor (title:string, category:string, ingredients:string, cookingTime:number, prep:string) {
    // TODO: Sanitize the string values to prevent injection
    this.title = title;
    this.category = category;
    this.ingredients = ingredients;
    this.cookingTime = cookingTime;
    this.prep = prep;
  }
}
