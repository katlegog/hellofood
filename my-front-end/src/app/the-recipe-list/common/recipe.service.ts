import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from './recipe';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private url = "http://localhost:3000/api/recipe";
  constructor(private http: HttpClient) { }

  createRecipe(body) {
    this.http.post(this.url, body).subscribe(response => {
      console.log(response);
    })
  }

  getRecipes() {
    return this.http.get(this.url);
  }

}
