import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { TheRecipeListComponent } from './the-recipe-list/the-recipe-list.component';

const routes: Routes = [
 { path: '', component: ContentComponent },
 { path: 'recipe', component: TheRecipeListComponent },
 { path: 'gallery', component: ContentComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
