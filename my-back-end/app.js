const express = require('express');
const app = express();
const morgan =require('morgan');
const bodyParser = require ('body-parser');
const mongoose = require('mongoose');
const recipeRoutes = require('./api/routes/recipes');
const enquireRoutes = require('./api/routes/enquire');



//connect to mongodb atlas
mongoose.connect('mongodb://Katlego:'+ process.env.MONGO_ATLAS_PW + '@cluster0-shard-00-00-iv1av.mongodb.net:27017,cluster0-shard-00-01-iv1av.mongodb.net:27017,cluster0-shard-00-02-iv1av.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'

);

//dev tool to log requests
app.use(morgan('dev'));

//body parser recieves form data
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//corse error handelers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
  if (req.method === "OPTIONS"){
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET ');
    return res.status(200).json({});
  }
  next();
});

/// routes that should handle requests
app.use('/recipes',recipeRoutes);
app.use('/enquire',enquireRoutes);

// error messages for undefined requests
app.use((req, res, next)=>{
const error = new Error('Not Found');
error.status= 404;
next(error);
});

app.use((error, req, res, next)=>{
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
