const express = require('express');

const router= express.Router();

router.get('/', (req, res, next)=>{
  res.status(200).json({
    message: 'Enquiries fetched'
  });
});

router.post('/', (req, res, next)=>{
  const equiry ={
    name: req.body.name,
    email: req.body.email,
    msg: req.body.msg

  };
  res.status(201).json({
    message: 'Enquiries created',
    enquiry: equiry
  });
});

router.get('/:enquiryId', (req, res, next)=>{
  res.status(201).json({
    message: 'Enquiries details',
    enquiryId : req.params.enquiryId
  });
});

router.delete('/:enquiryId', (req, res, next)=>{
  res.status(201).json({
    message: 'Enquiries deleted',
    enquiryId : req.params.enquiryId
  });
});

module.exports = router;
