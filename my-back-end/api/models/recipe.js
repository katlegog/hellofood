const mongoose = require('mongoose');

const recipeSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  title: String,
  category: String,
  ingredients: String,
  cookingTime: Number,
  prep: String
});

module.exports = mongoose.model('Recipe',recipeSchema);
